import { Grid } from './util';

class Sudoku {
    private grid: Grid;

    constructor(private N: number = 9, private K: number) {
        // Initialize grid with correct dimensions and default values
        this.grid = Array.from({ length: N * N }, (_, index) => ({
            value: 0,
            row: Math.floor(index / N),
            column: index % N,
            square: Math.floor((index / N) / 3) * 3 + Math.floor((index % N) / 3),
            editable: true,
        }));
    }

    // Shuffle array utility
    private shuffle(array: number[]): void {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
    }

    private checkIfSafe(i: number, num: number): boolean {
        const row = Math.floor(i / this.N);
        const col = i % this.N;
        const squareStartRow = row - row % 3;
        const squareStartCol = col - col % 3;

        for (let j = 0; j < this.N; j++) {
            const rowCheck = this.grid[row * this.N + j].value === num;
            const colCheck = this.grid[j * this.N + col].value === num;
            const squareRow = squareStartRow + Math.floor(j / 3);
            const squareCol = squareStartCol + (j % 3);
            const squareCheck = this.grid[squareRow * this.N + squareCol].value === num;
            if (rowCheck || colCheck || squareCheck) {
                return false;
            }
        }
        return true;
    }

    private fillRemaining(i: number): boolean {
        if (i >= this.N * this.N) {
            return true;
        }

        if (this.grid[i].value !== 0) {
            return this.fillRemaining(i + 1);
        }

        const numbers = Array.from({ length: this.N }, (_, index) => index + 1);
        this.shuffle(numbers);

        for (let num of numbers) {
            if (this.checkIfSafe(i, num)) {
                this.grid[i].value = num;
                if (this.fillRemaining(i + 1)) {
                    return true;
                }
                this.grid[i].value = 0;
            }
        }
        return false;
    }

    private removeKDigits(): void {
        let count = this.K;
        while (count !== 0) {
            const i = Math.floor(Math.random() * this.N * this.N);
            if (this.grid[i].value !== 0) {
                this.grid[i].value = 0;
                count--;
            }
        }
    }

    public generate(): Grid {
        this.fillRemaining(0);
        this.removeKDigits();
        return this.grid;
    }

    public solve(): boolean {
        return this.fillRemaining(0);
    }

    public setGrid(grid: Grid): void {
        this.grid = grid;
    }

    public getGrid(): Grid {
        return this.grid;
    }
}

export default Sudoku;
