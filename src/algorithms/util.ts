export interface Cell {
    value: number;
    row: number;
    column: number;
    square: number;
    editable: boolean; 
}
  
export type Grid = Cell[];

// Function to determine the square number based on row and column
export function squareNumber(row: number, column: number): number {
  if (row < 3 && column < 3) return 0;
  if (row < 3 && column < 6) return 3;
  if (row < 3) return 6;
  if (row < 6 && column < 3) return 1;
  if (row < 6 && column < 6) return 4;
  if (row < 6) return 7;
  if (column < 3) return 2;
  if (column < 6) return 5;
  return 8;
}

// Function to initialize a blank Sudoku grid
export function initialGrid(): Grid {
    let grid: Grid = [];
    for (let row = 0; row < 9; row++) {
      for (let col = 0; col < 9; col++) {
        grid.push({
          value: 0,  // Start with an empty cell
          row: row,
          column: col,
          square: squareNumber(row, col),
          editable: true  // All cells are editable initially
        });
      }
    }
    return grid;
  }