import React, { useState } from 'react';
import SudokuGrid from './ui/Grid';
import Buttons from './ui/Buttons';
import { Grid, initialGrid } from './algorithms/util';
import './App.css';
import ThemeSwitch from './ui/ThemeSwitch';
import Sudoku from './algorithms/Sudoku';
import DifficultySelector from './ui/DifficultySelector';

const App: React.FC = () => {
  const [grid, setGrid] = useState<Grid>(initialGrid());
  const [difficulty, setDifficulty] = useState('easy');

  const handleGenerate = () => {
    let cells: number = 0;
    if(difficulty === 'easy') cells = 20
    if(difficulty === 'medium') cells = 30
    if(difficulty === 'hard') cells = 50
    const sudoku = new Sudoku(9, cells);
    setGrid(sudoku.generate());
  };

  const handleSolve = () => {
    const sudoku = new Sudoku(9, 0); // K is irrelevant here
    sudoku.setGrid(grid); // Assuming you add a method to set the grid
    if (sudoku.solve()) {
        setGrid([...sudoku.getGrid()]); // Deep copy to trigger React update
    } else {
      console.log("No solution found or no changes needed");
    }
  };

  const handleClear = () => {
    setGrid(initialGrid());
  };

  const handleDifficultyChange = (newDifficulty: React.SetStateAction<string>) => {
    setDifficulty(newDifficulty);
    console.log(newDifficulty)
  };

  return (
    <>
      <ThemeSwitch />
      <div className="container">
        <h1>Sudoku Solver</h1>
        <Buttons onGenerate={handleGenerate} onSolve={handleSolve} onClear={handleClear} />
        <SudokuGrid grid={grid} setGrid={setGrid} />
        <DifficultySelector currentDifficulty={difficulty} onDifficultyChange={handleDifficultyChange} />
      </div>
    </>
  );
};

export default App;
