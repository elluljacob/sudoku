import React from 'react';

interface ButtonsProps {
  onGenerate: () => void;
  onSolve: () => void;
  onClear: () => void;
}

const Buttons: React.FC<ButtonsProps> = ({ onGenerate, onSolve, onClear }) => {
  return (
    <div className="buttons">
      <button onClick={onGenerate}>Generate Sudoku</button><br />
      <button onClick={onSolve}>Solve Sudoku</button><br />
      <button onClick={onClear}>Clear Board</button>
    </div>
  );
};

export default Buttons;
