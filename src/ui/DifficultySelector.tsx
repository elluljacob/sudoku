const DifficultySelector = ({ onDifficultyChange, currentDifficulty }: { onDifficultyChange: (difficulty: string) => void, currentDifficulty: string }) => {
  return (
    <div className="radio-input">
    <label className="difficulty-label">{currentDifficulty.charAt(0).toUpperCase() + currentDifficulty.slice(1)}</label>
      <input checked={currentDifficulty === 'easy'} onChange={() => onDifficultyChange('easy')} value="easy" name="difficulty" id="color-1" type="radio" />
      <label htmlFor="color-1">
        <span><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"><path stroke-linejoin="round" stroke-linecap="round" stroke-width="2" stroke="#ffffff" d="M6 12L10.2426 16.2426L18.727 7.75732"></path></svg></span>
      </label>
      
      <input checked={currentDifficulty === 'medium'} onChange={() => onDifficultyChange('medium')} value="medium" name="difficulty" id="color-2" type="radio" />
      <label htmlFor="color-2">
        <span><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"><path stroke-linejoin="round" stroke-linecap="round" stroke-width="2" stroke="#ffffff" d="M6 12L10.2426 16.2426L18.727 7.75732"></path></svg></span>
      </label>
      
      <input checked={currentDifficulty === 'hard'} onChange={() => onDifficultyChange('hard')} value="hard" name="difficulty" id="color-3" type="radio" />
      <label htmlFor="color-3">
        <span><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"><path stroke-linejoin="round" stroke-linecap="round" stroke-width="2" stroke="#ffffff" d="M6 12L10.2426 16.2426L18.727 7.75732"></path></svg></span>
      </label>
      <div className="tooltip">
        <div className="icon">i</div>
        <div className="tooltiptext">Select Difficulty for Generator</div>
    </div>
    </div>
  );
};

export default DifficultySelector;
