import React from 'react';
import { Cell as CellType } from '../algorithms/util';

interface SudokuCellProps {
  cell: CellType;
  onChange: (newValue: number) => void;
}

const SudokuCell: React.FC<SudokuCellProps> = ({ cell, onChange }) => {
  return (
    <input
      type="text"
      maxLength={1}
      value={cell.value === 0 ? "" : cell.value.toString()}
      onChange={e => {
        const val = parseInt(e.target.value, 10);
        if (!isNaN(val) && val > 0 && val < 10) {
          onChange(val);
        } else if (e.target.value === "") {
          onChange(0);
        }
      }}
      className="sudoku-cell"
      disabled={!cell.editable}
    />
  );
};

export default SudokuCell;
