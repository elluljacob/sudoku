import React from 'react';
import SudokuCell from './Cell';
import { Grid } from '../algorithms/util';

interface SudokuGridProps {
  grid: Grid;
  setGrid: React.Dispatch<React.SetStateAction<Grid>>;
}

const SudokuGrid: React.FC<SudokuGridProps> = ({ grid, setGrid }) => {
  const handleCellChange = (index: number, value: number) => {
    const newGrid = [...grid];
    newGrid[index].value = value;
    setGrid(newGrid);
  };

  return (
    <table className="sudoku-table">
      <tbody>
        {Array.from({ length: 9 }, (_, rowIndex) => (
          <tr key={rowIndex}>
            {grid.slice(rowIndex * 9, rowIndex * 9 + 9).map((cell, colIndex) => (
              <td key={colIndex}>
                <SudokuCell cell={cell} onChange={value => handleCellChange(rowIndex * 9 + colIndex, value)} />
              </td>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default SudokuGrid;
